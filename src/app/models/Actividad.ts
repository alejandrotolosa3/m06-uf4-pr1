export interface Actividad {
    id: string;
    nombre: string;
    fecha: string;
    prediccion: string;
}
