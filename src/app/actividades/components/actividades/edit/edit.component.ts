import { Component, OnInit } from '@angular/core';
import {ActividadService} from '../../../services/actividad.service';
declare const M: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public actividad = { id: 'J9XCnFlg7Y8EtGrU5sHK', nombre: "Surf", fecha: "14/04/2020", prediccion: "Nublado"};
  public show_toast;
  constructor(private actividadservice: ActividadService) { }

  ngOnInit(): void {
    document.addEventListener('DOMContentLoaded', function() {
      const options = {
        format: 'dd/mm/yyyy',
      };
      var elems = document.querySelectorAll('.datepicker');
      M.Datepicker.init(elems, options);
    })
}

  action(){
    //SI ALGUN CAMPO DEL FORMULARIO ESTA VACIO MOSTRAR ERROR SINO HACER EL UPDATE EN LA BBDD
    if(false){
      M.toast({html: 'Algun campo en el formulario esta vacio'})
    }else{
      this.show_toast= this.actividadservice.updateActividad(this.actividad);
    }
  }

}
