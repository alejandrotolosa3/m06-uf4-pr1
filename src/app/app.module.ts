import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { PageNotFoundComponent } from './actividades/components/page-not-found/page-not-found.component';
import { ShowComponent } from './actividades/components/actividades/show/show.component';
import { CreateComponent } from './actividades/components/actividades/create/create.component';
import { EditComponent } from './actividades/components/actividades/edit/edit.component';
import { DeleteComponent } from './actividades/components/actividades/delete/delete.component';
import { MaterializeButtonModule, MaterializeCardModule } from 'materialize-angular';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ShowComponent,
    CreateComponent,
    EditComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AppRoutingModule,
    MaterializeButtonModule,
    MaterializeCardModule,
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
