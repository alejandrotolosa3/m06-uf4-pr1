import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Actividad } from '../../models/Actividad';


@Injectable({
  providedIn: 'root'
})
export class ActividadService {

  constructor(private firestore: AngularFirestore) { }ç


    // CREAR ACTIVIDAD EN BASE DE DATOS
    public createActividad(actividad: any) {


      return this.firestore.collection('actividades').add(actividad);

      
      // return new Promise<any>((resolve, reject) =>{
      //     this.firestore
      //         .collection("actividades")
      //         .add({ id: actividad.id, nombre: actividad.nombre, fecha: actividad.fecha, prediccion: actividad.fecha})
      //         .then(res => {}, err => reject(err));
      // });
    }
  
    // GET ACTIVIDADES DE BASE DE DATOS
    getActividades() {
      return this.firestore.collection("actividades").snapshotChanges();
    }

    async deleteActividad(key: string){
      return new Promise<any>((resolve, reject) =>{
              this.firestore
              .collection("actividades")
              .doc(key)
              .delete()
              .then(res => {true}, err => reject(err));
      });
} 

async updateActividad(actividad: Actividad){
  console.log (actividad.fecha);
  return this.firestore.collection('actividades').doc(actividad.id).set(actividad);
  } 

}
