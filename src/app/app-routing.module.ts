import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageNotFoundComponent} from './actividades/components/page-not-found/page-not-found.component';
import {ShowComponent} from './actividades/components/actividades/show/show.component';
import {CreateComponent} from './actividades/components/actividades/create/create.component';
import {EditComponent} from './actividades/components/actividades/edit/edit.component';
import {DeleteComponent} from './actividades/components/actividades/delete/delete.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
 // { path: 'login', component: ListuserComponent},
  //CREATE, SHOW, EDIT Y DELETE DEBEN SER HIJOS DE ALGUN COMPONENTE
  { path: 'show', component: ShowComponent},
  { path: 'create', component: CreateComponent},
  { path: 'edit/:id', component: EditComponent},
  { path: 'delete/:id', component: DeleteComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
