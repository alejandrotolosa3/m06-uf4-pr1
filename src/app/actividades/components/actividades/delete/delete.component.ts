import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ActividadService} from '../../../services/actividad.service';
declare const M: any;

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})

export class DeleteComponent implements OnInit {
  public actividad = { id: 'J9XCnFlg7Y8EtGrU5sHK', nombre: "Surf", fecha: "14-04-2020", prediccion: "Soleado"};
  public show_toast;
  // public actividad_id;
  constructor(private route:ActivatedRoute, private actividadservice: ActividadService) { }

  ngOnInit(): void {
    // this.actividad_id=this.route.snapshot.paramMap.get('id');
    // console.log(this.actividad_id);
  }

  async action(){
    console.log(this.actividad.id)
    this.show_toast= this.actividadservice.deleteActividad(this.actividad.id);
    console.log(this.show_toast);
    
    if (this.show_toast==true){
      M.toast({html: 'Eliminado'})
    }else{
      M.toast({html: 'No se ha podido eliminar'})
    }
  }



}
