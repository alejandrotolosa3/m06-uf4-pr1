// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB8AiORohFwbxTjcgk-eQJxlaqktgyA8ms",
    authDomain: "m06-uf4-pr1.firebaseapp.com",
    databaseURL: "https://m06-uf4-pr1.firebaseio.com",
    projectId: "m06-uf4-pr1",
    storageBucket: "m06-uf4-pr1.appspot.com",
    messagingSenderId: "842474440066",
    appId: "1:842474440066:web:25589e7ac1aa4636b8fb9b",
    measurementId: "G-3QDW53QYL9"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
