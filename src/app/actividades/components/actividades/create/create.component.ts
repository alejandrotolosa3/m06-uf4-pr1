import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Actividad } from 'src/app/models/Actividad';
import { ActividadService } from 'src/app/actividades/services/actividad.service';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  nombre: string;
  fecha: Date;
  errorMessage: string;

  constructor(private actividadService: ActividadService, private router: Router) { }
  public actividad = { id: 'sdf90sdfs89dfsd09', nombre: "Surf", fecha: "14-04-2020", prediccion: "Soleado"};
  ngOnInit(): void {
  }

  async create() {

    try {
      console.log("HOLA")
      await this.actividadService.createActividad(this.actividad);
    } catch (e) {
      if (e.status === 400) {
        this.errorMessage = 'No se pudo crear la actividad';
      } else {
        console.log(e.status, e.message);
      }
    }

    // console.log("Nombre: " + this.nombre + ", Fecha: " + this.fecha);
    
  }

}
